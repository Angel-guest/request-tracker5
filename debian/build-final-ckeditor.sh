#!/bin/bash
#
# Run first: apt-get install ckbuilder

set -e

THIRD_PARTY_DIR=third-party-source/devel/third-party
SRC=$THIRD_PARTY_DIR/ckeditor-src
SKINSRC=$THIRD_PARTY_DIR/bootstrapck-src
CCMSCONFIGHELPERSRC=$THIRD_PARTY_DIR/ccmsconfighelper
CONFIGHELPERSRC=$THIRD_PARTY_DIR/confighelper-src
DOCFONTSRC=$THIRD_PARTY_DIR/docfont
DEST=ckeditor-build
FINALDEST=share/static/RichText
BUILD_CONFIG=$THIRD_PARTY_DIR/ckeditor-4.20.1/build-config.js

rm -rf $DEST

## Copy aux sources into main source

mkdir $SRC/skins
cp -a $SKINSRC/skins/bootstrapck $SRC/skins/
cp -a $CCMSCONFIGHELPERSRC $SRC/plugins/
cp -a $CONFIGHELPERSRC $SRC/plugins/confighelper
cp -a $DOCFONTSRC $SRC/plugins/

# --add-exports is from https://github.com/ckeditor/ckbuilder/issues/34
java --add-exports java.desktop/sun.java2d=ALL-UNNAMED -jar /usr/bin/ckbuilder \
    --build $SRC $DEST --skip-omitted-in-build --build-config $BUILD_CONFIG

(
    cd $DEST/ckeditor
    rm -rf .github CHANGES.md LICENSE.md README.md samples
    mv ckeditor.js ckeditor.min.js
)

## Copy results into final location

# Preserve customised config file
cp -p $FINALDEST/{config.js,contents.css,contents-dark.css} $DEST/ckeditor/
rm -rf $FINALDEST
cp -a $DEST/ckeditor/ $FINALDEST/
rm -rf $DEST

rm -rf $SRC/skins/bootstrapck $SRC/plugins/ccmsconfighelper $SRC/plugins/confighelper $SRC/plugins/docfont
