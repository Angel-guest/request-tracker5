#!/bin/sh
set -e

branch_version=5
branch_priority=240

warn() {
	fmt -60 | sed 's/^/**WARNING**  /' 1>&2
}

warn <<eof

If you are using mod_perl or any form of persistent perl process
such as FastCGI, you will need to restart your web server and any
persistent processes now.

For mod_perl this means

invoke-rc.d apache2 stop && invoke-rc.d apache2 start

eof

find /var/cache/request-tracker$branch_version/mason_data -type f -print0 |xargs -r0 rm -f

alts() {
    for x in "/usr/bin/rt-crontool 1" "/usr/sbin/rt-setup-database 8" \
             "/usr/sbin/rt-dump-initialdata 8" \
             "/usr/sbin/rt-dump-metadata 8" \
             "/usr/sbin/rt-email-digest 8" \
             "/usr/sbin/rt-email-dashboards 8" \
             "/usr/sbin/rt-clean-sessions 8" \
             "/usr/sbin/rt-shredder 8" \
             "/usr/sbin/rt-email-group-admin 8" \
             "/usr/sbin/rt-attributes-viewer 8" \
             "/usr/sbin/rt-fulltext-indexer 8" \
             "/usr/sbin/rt-setup-fulltext-index 8" \
             "/usr/sbin/rt-validator 8" \
             "/usr/sbin/rt-session-viewer 8" \
             "/usr/sbin/rt-preferences-viewer 8" \
             "/usr/sbin/rt-validate-aliases 8" \
             "/usr/sbin/rt-importer 8" \
             "/usr/sbin/rt-serializer 8" \
             "/usr/sbin/rt-externalize-attachments 8" \
             "/usr/sbin/rt-ldapimport 8" \
             "/usr/sbin/rt-passwd 8" \
             "/usr/sbin/rt-clean-attributes 8" \
             "/usr/sbin/rt-clean-shorteners 8" \
             "/usr/sbin/rt-munge-attachments 8" \
             "/usr/sbin/rt-search-attributes 8"; do
        set -- $x; alt=$1; manext=$2; base=`basename $alt`
        update-alternatives \
	  --install $alt $base $alt-$branch_version $branch_priority \
          --slave /usr/share/man/man$manext/$base.$manext.gz \
	          $base.$manext.gz \
                  /usr/share/man/man$manext/$base-$branch_version.$manext.gz
    done
}

create_debconf_snippet () {
    # create a configuration snippet from the debconf database
    # and handle it with ucf
    tfile=$(mktemp -t request-tracker5-config.XXXXXXXX) || exit 1
    for i in rtname organization correspondaddress commentaddress \
             webpath webbaseurl
    do
        db_get "request-tracker5/$i"
        echo "$i=$RET"
    done | /usr/share/request-tracker5/debian/write-siteconfig \
           /usr/share/request-tracker5/debian/siteconfig.template > $tfile
    ucf --debconf-ok $tfile /etc/request-tracker5/RT_SiteConfig.d/50-debconf.pm
    rm $tfile
}

run_dbconfig () {
    dbc_generate_include=template:/etc/request-tracker5/RT_SiteConfig.d/51-dbconfig-common.pm
    dbc_generate_include_args="-o template_infile=/usr/share/request-tracker5/debian/dbconfig.template"
    if [ "$HANDLE_PERMISSIONS" = "true" ]
    then
        # hint the SQLite code for the database file permissions
        dbc_dbfile_owner="root:www-data"
        dbc_dbfile_perms=0660
    fi

    ROOT_PASSWORD="password"
    db_get request-tracker5/dbconfig-install
    DBC_INSTALL=$RET
    if [ -n "$2" ]; then
        DBC_INSTALL="false"
    fi
    db_get request-tracker5/dbconfig-reinstall
    DBC_REINSTALL=$RET
    if [ "$DBC_INSTALL" = "true" -o "$DBC_REINSTALL" = "true" ]; then
        db_get request-tracker5/initial-root-password
        if [ -n "$RET" ]; then
            ROOT_PASSWORD="$RET"
        fi
    fi
    ROOT_PASSWORD_FILE=`mktemp`
    export ROOT_PASSWORD_FILE
    echo $ROOT_PASSWORD > $ROOT_PASSWORD_FILE
    unset ROOT_PASSWORD
    if [ ! -e /usr/share/dbconfig-common/internal/dbc-no-thanks ]; then
        . /usr/share/dbconfig-common/dpkg/postinst
    fi
    dbc_go request-tracker5 $@
    if [ "$DBC_INSTALL" = "true" -o "$DBC_REINSTALL" = "true" ]; then
        db_reset request-tracker5/initial-root-password
        db_fset request-tracker5/initial-root-password seen true
        rm -f $ROOT_PASSWORD_FILE
    fi
}

maybe_handle_permissions () {
    if [ "$HANDLE_PERMISSIONS" = "true" ]
    then
        for f in /etc/request-tracker5/RT_SiteConfig.d/50-debconf.pm \
                 /etc/request-tracker5/RT_SiteConfig.d/51-dbconfig-common.pm; do
            if [ -f "$f" ]; then
                chown root:www-data $f && chmod 640 $f
            fi
        done
    fi
}

ucf_register () {
    # Add the generated files into the ucf registry
    if which ucfr >/dev/null 2>&1
    then
        ucfr request-tracker5 /etc/request-tracker5/RT_SiteConfig.d/50-debconf.pm
        ucfr request-tracker5 /etc/request-tracker5/RT_SiteConfig.pm
        ucfr request-tracker5 /etc/cron.d/request-tracker5
        # this should probably be registered by dbconfig-generate-include,
        # but ucfr is idempotent so it doesn't hurt here anyway.
        ucfr request-tracker5 /etc/request-tracker5/RT_SiteConfig.d/51-dbconfig-common.pm
    fi
}

setup_cronjobs () {
    tfile=$(mktemp -t request-tracker5-config.XXXXXXXX) || exit 1
    chmod 0755 $tfile
    if [ "$INSTALL_CRONJOBS" = "true" ]
    then
        cat > $tfile <<EOF
0 0 * * * www-data [ -x /usr/sbin/rt-email-digest-5 ] && /usr/sbin/rt-email-digest-5 -m daily
0 0 * * 0 www-data [ -x /usr/sbin/rt-email-digest-5 ] && /usr/sbin/rt-email-digest-5 -m weekly
0 * * * * www-data [ -x /usr/sbin/rt-email-dashboards-5 ] && /usr/sbin/rt-email-dashboards-5
0 * * * * www-data [ -x /usr/sbin/rt-clean-attributes-5 ] && /usr/sbin/rt-clean-attributes-5
0 * * * * www-data [ -x /usr/sbin/rt-clean-shorteners-5 ] && /usr/sbin/rt-clean-shorteners-5
# Enable the following cron job if you have set up external storage
#0 0 * * * www-data [ -x /usr/sbin/rt-externalize-attachments-5 ] && /usr/sbin/rt-externalize-attachments-5
EOF
    else
        cat > $tfile <<EOF
#0 0 * * * www-data [ -x /usr/sbin/rt-email-digest-5 ] && /usr/sbin/rt-email-digest-5 -m daily
#0 0 * * 0 www-data [ -x /usr/sbin/rt-email-digest-5 ] && /usr/sbin/rt-email-digest-5 -m weekly
#0 * * * * www-data [ -x /usr/sbin/rt-email-dashboards-5 ] && /usr/sbin/rt-email-dashboards-5
#0 * * * * www-data [ -x /usr/sbin/rt-clean-attributes-5 ] && /usr/sbin/rt-clean-attributes-5
#0 * * * * www-data [ -x /usr/sbin/rt-clean-shorteners-5 ] && /usr/sbin/rt-clean-shorteners-5
## Enable the following cron job if you have set up external storage
##0 0 * * * www-data [ -x /usr/sbin/rt-externalize-attachments-5 ] && /usr/sbin/rt-externalize-attachments-5
EOF
    fi
    mkdir -p /etc/cron.d
    ucf --debconf-ok $tfile /etc/cron.d/request-tracker5
    rm $tfile
}

copy_gpg_data() {
    if [ -d /var/cache/request-tracker4/data/gpg ]; then
        echo "Copyng GPG data from /var/lib/request-tracker4"
        # This should be part of the package, but make sure
        if [ ! -d /var/lib/request-tracker5/data ]; then
            mkdir -p /var/lib/request-tracker5/data
            chown www-data:root /var/lib/request-tracker5/data
            chown 2750 /var/lib/request-tracker5/data
        fi
        cp -vu /var/cache/request-tracker5/data/gpg/* \
               /var/lib/request-tracker5/data/gpg
    fi
}

# The actual work starts here
. /usr/share/debconf/confmodule

# this is used for the SiteConfig.pm file and the possible SQLite database file
db_get request-tracker5/handle-siteconfig-permissions
HANDLE_PERMISSIONS="$RET"

db_get request-tracker5/install-cronjobs
INSTALL_CRONJOBS="$RET"

create_debconf_snippet
if [ ! -e /usr/share/dbconfig-common/internal/dbc-no-thanks ]; then
    run_dbconfig $@
fi

# Update /etc/request-tracker5/RT_SiteConfig.pm. This is now static
# content, but we keep managing it with ucf to avoid unnecessary user
# prompting. This is a quick fix to resolve an RC bug just before freeze;
# there is probably a way to transition from ucf to a conffile that should
# be investigated later.
#
# This is a much-shortened version of the previous update-rt-siteconfig
# script (embedded as there should be no reason to call it elsewhere now.
SITE_CONFIG="/etc/request-tracker5/RT_SiteConfig.pm"
tfile=$(mktemp -t request-tracker-siteconfig.XXXXXXXX) || exit 1
cat <<EOF > $tfile
# Default RT_SiteConfig.pm for Debian
# Since RT 4.4, snippets from /etc/request-tracker*/RT_SiteConfig.d
# are read in by RT itself, so this file is now empty.
EOF
if [ -f "$SITE_CONFIG" ]
then
    # try to honor the existing mode and owner of the file
    chown --reference "$SITE_CONFIG" $tfile
    chmod --reference "$SITE_CONFIG" $tfile
fi
ucf --debconf-ok $tfile "$SITE_CONFIG"
rm $tfile

maybe_handle_permissions
setup_cronjobs
ucf_register
copy_gpg_data $@

case "$1" in
    configure)
        alts
        ;;
    abort-upgrade)
        alts
        ;;
esac


#DEBHELPER#
