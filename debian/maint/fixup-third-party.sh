#!/bin/bash

set -e

RTVER=5.0.4
THIRD_PARTY_TARBALL_DIR=../../../rt-$RTVER/devel/third-party
SRC=$THIRD_PARTY_TARBALL_DIR/ckeditor-src
SKINSRC=$THIRD_PARTY_TARBALL_DIR/bootstrapck-src
CCMSCONFIGHELPERSRC=$THIRD_PARTY_TARBALL_DIR/ccmsconfighelper
CONFIGHELPERSRC=$THIRD_PARTY_TARBALL_DIR/confighelper-src
DOCFONTSRC=$THIRD_PARTY_TARBALL_DIR/docfont
PLUGIN_LIST=ckeditor-plugins.txt

if [ -z "$6" ]; then
    echo "Usage: $0 <path-to-clean-checkout-of-ckeditor> <path to-clean-checkout-of-BootstrapCK4-Skin> <path-to-clean-checkout-of-confighelper> <path to ccmsconfighelper> <path to <docfont> <version of ckeditor to use>"
    exit 1
fi

CHECKOUT=$1 # git@github.com:ckeditor/ckeditor4.git
SKINCHECKOUT=$2 # git@github.com:Kunstmaan/BootstrapCK4-Skin.git
CONFIGHELPERCHECKOUT=$3 # git@github.com:AlfonsoML/confighelper.git
CCMSCONFIGHELPERCHECKOUT=$4 # manually downloaded from https://ckeditor.com/cke4/addon/ccmsconfighelper
DOCFONTCHECKOUT=$5 # manually downloaded from https://ckeditor.com/cke4/addon/docfont
CKEDITORVER=$6

## Prepare sources

(
    cd $CHECKOUT
    git fetch
    git checkout $CKEDITORVER
)

rm -rf $SRC
cp -a $CHECKOUT/ $SRC/
rm -rf $SRC/.git

rm -rf $SKINSRC
cp -a $SKINCHECKOUT/ $SKINSRC/
rm -rf $SKINSRC/.git

rm -rf $CONFIGHELPERSRC
cp -a $CONFIGHELPERCHECKOUT/ $CONFIGHELPERSRC/
rm -rf $CONFIGHELPERSRC/.git

rm -rf $CCMSCONFIGHELPERSRC
cp -a $CCMSCONFIGHELPERCHECKOUT $CCMSCONFIGHELPERSRC

rm -rf $DOCFONTSRC
cp -a $DOCFONTCHECKOUT $DOCFONTSRC

## Tidy up sources

# $PLUGIN_LIST is manually curated - it includes deps not listed in 
# build-config.js
comm -1 -3 <(sort $PLUGIN_LIST) <(ls -1 $SRC/plugins/|sort ) | while read p; do
    rm -rf $SRC/plugins/$p
done
rm -rf $SRC/{skins,tests}

rm -rf $SKINSRC/{plugins,skins/bootstrapck-dev,skins/ckbuilder.jar}
